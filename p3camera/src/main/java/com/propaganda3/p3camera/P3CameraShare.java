package com.propaganda3.p3camera;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.propaganda3.p3camera.ui.fragment.ShareImageFragment;
import com.propaganda3.p3camera.utility.P3CameraConstant;

/**
 * Created by nikhil on 19/10/15.
 */
public class P3CameraShare {
    private static ShareImageFragment shareImageFragment = new ShareImageFragment();

    /**
     * Sets HashTag text and font
     *
     * @param context
     * @param hashTag Hash-Tag text
     * @param hashTagTextColor color to be set to the text
     * @param hashTagTextTypeFace typeface to be set to the fonts
     * @param hashTagTextSize text size in sp
     */
    public static void setHashTag(Context context, String hashTag, int hashTagTextColor, Typeface hashTagTextTypeFace, int hashTagTextSize) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
       shareImageFragment.setHashTag(hashTag, ContextCompat.getColor(context,hashTagTextColor), hashTagTextTypeFace, hashTagTextSize);
    }

    /**
     * Sets caption hint text and fonts
     *
     * @param context
     * @param hintText hint text to be displayed
     * @param hintTextColor the color of the text hint
     * @param textColor the color of the text
     * @param typeface typeface to be set to the fonts
     * @param textSize text size in sp
     */
    public static void setCaptionTextFont(Context context, String hintText, int hintTextColor, int textColor, Typeface typeface, int textSize) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        shareImageFragment.setCaptionTextFont(hintText, ContextCompat.getColor(context, hintTextColor), ContextCompat.getColor(context, textColor), typeface, textSize);
    }

    /**
     * Sets toast message when image is saved successfully.
     * @param toastMessageImageSaved message to be displayed
     */
    public static void setToastMessageImageSaved(String toastMessageImageSaved) {
       shareImageFragment.setToastMessageImageSaved(toastMessageImageSaved);
    }

    /**
     * Sets background color for the screen
     * @param context
     * @param backgroundColorScreen the color of the background
     */
    public static void setBackgroundColorScreen(Context context, int backgroundColorScreen) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        shareImageFragment.setBackgroundColorScreen(ContextCompat.getColor(context, backgroundColorScreen));
    }

    /**
     * Sets background color for the layout containing image and the caption
     * @param context
     * @param backgroundColorImageAndCaption - the color of the background
     */
    public static void setBackgroundColorImageAndCaption(Context context, int backgroundColorImageAndCaption) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        shareImageFragment.setBackgroundColorImageAndCaption(ContextCompat.getColor(context, backgroundColorImageAndCaption));
    }

    /**
     * Sets Album icon and makes this option visible in the UI
     * @param iconResourceId the resource identifier of the drawable for album
     * @param saveToAlbumDirectoryPath the path where the image needs to be saved
     * @param saveToAlbumImageName the image name by which the image is to be saved
     */
    public static void setAlbumIcon(int iconResourceId, String saveToAlbumDirectoryPath, String saveToAlbumImageName) {
        shareImageFragment.setAlbumIcon(iconResourceId, saveToAlbumDirectoryPath, saveToAlbumImageName);
    }

    /**
     * Sets Album icon and makes this option visible in the UI
     * @param iconResourceId the resource identifier of the drawable for album
     * @param saveToAlbumDirectoryPath the path where the image needs to be saved
     */
    public static void setAlbumIcon(int iconResourceId, String saveToAlbumDirectoryPath) {
        shareImageFragment.setAlbumIcon(iconResourceId, saveToAlbumDirectoryPath, null);
    }

    /**
     * Sets Email icon and makes this share option visible in the UI
     * @param iconResourceId the resource identifier of the drawable for email
     */
    public static void setEmailIcon(int iconResourceId) {
        shareImageFragment.setEmailIcon(iconResourceId);
    }

    /**
     * Sets Twitter icon and makes this share option visible in the UI
     * @param iconResourceId the resource identifier of the drawable for Twitter
     */
    public static void setTwitterIcon(int iconResourceId) {
        shareImageFragment.setTwitterIcon(iconResourceId);
    }

    /**
     * Sets Facebook icon and makes this share option visible in the UI
     * @param iconResourceId the resource identifier of the drawable for Facebook
     */
    public static void setFacebookIcon(int iconResourceId) {
        shareImageFragment.setFacebookIcon(iconResourceId);
    }

    /**
     *  Sets Instagram icon and makes this share option visible in the UI
     * @param iconResourceId  the resource identifier of the drawable for Instagram
     */
    public static void setInstagramIcon(int iconResourceId) {
        shareImageFragment.setInstagramIcon(iconResourceId);
    }

    /**
     * Loads Share Image fragment into the containerView that is supplied.
     *
     * @param containerViewId Id of the container layout in which the fragment is to be loaded.
     * @param fragmentTransaction if loading from Activity - Pass getSupportFragmentManager().beginTransaction() and
     *                            if loading inside another fragment - Pass getChildFragmentManager().beginTransaction()
     * @param savedImagePath path of the saved image from Edit image screen
     */
    public static void loadShareImageFragment(int containerViewId, FragmentTransaction fragmentTransaction, String savedImagePath) {
        //shareImageFragment.setSaveToAlbumDirectoryPath(saveToAlbumDirectoryPath);
        //shareImageFragment.setSaveToAlbumImageName(saveToAlbumImageName);

        Bundle args = new Bundle();
        args.putString(P3CameraConstant.INTENT_EXTRA_IMAGE_PATH, savedImagePath);
        shareImageFragment.setArguments(args);
        fragmentTransaction.replace(containerViewId, shareImageFragment);
        fragmentTransaction.commit();
    }
}
