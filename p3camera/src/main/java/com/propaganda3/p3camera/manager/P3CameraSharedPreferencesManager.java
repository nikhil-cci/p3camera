package com.propaganda3.p3camera.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.propaganda3.p3camera.utility.P3CameraConstant;

import java.util.UUID;

/**
 * Created by nikhil on 15/10/15.
 */
public class P3CameraSharedPreferencesManager {
    public static void setPrefStorageFolderName(Context context, String folderName){
        SharedPreferences sharedPreference = context.getSharedPreferences(P3CameraConstant.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreference.edit().putString(P3CameraConstant.SHARED_PREFERENCES_KEY_STORAGE_FOLDER, folderName).commit();
    }

    public static String getPrefStorageFolderName(Context context){

        SharedPreferences sharedPreference = context.getSharedPreferences(P3CameraConstant.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String folderName = sharedPreference.getString(P3CameraConstant.SHARED_PREFERENCES_KEY_STORAGE_FOLDER,null);
        if(folderName == null){
            //Generate a random String as a folder name to store camera image temporarily.
            folderName = UUID.randomUUID().toString();
            setPrefStorageFolderName(context,folderName);
        }
        return folderName;
    }
}
