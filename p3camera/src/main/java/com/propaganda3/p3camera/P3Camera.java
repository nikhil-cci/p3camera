package com.propaganda3.p3camera;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.propaganda3.p3camera.ui.fragment.CameraFragment;

/**
 * Created by nikhil on 09/10/15.
 */
public class P3Camera {

    private static CameraFragment cameraFragment = new CameraFragment();

    /**
     * Sets the resolution of the output image
     *
     * @param photoHeight height of the output image
     * @param photoWidth  width of the output image
     */
    public static void setImageResolution(int photoHeight, int photoWidth) {
        cameraFragment.setPhotoHeight(photoHeight);
        cameraFragment.setPhotoWidth(photoWidth);
    }

    /**
     * Sets icon/image for Switch Camera button
     *
     * @param resourceId resource identifier of the drawable to be set
     */
    public static void setSwitchCameraImageResource(int resourceId) {
        cameraFragment.setSwitchCameraResourceIdStateNormal(resourceId);
    }

    /**
     * Sets icon/image for Switch Camera capture button
     *
     * @param resourceIdStateNormal  the resource identifier of the drawable for button state normal
     * @param resourceIdStatePressed the resource identifier of the drawable for button state pressed
     */
    public static void setSwitchCameraImageResource(int resourceIdStateNormal, int resourceIdStatePressed) {
        cameraFragment.setSwitchCameraResourceIdStateNormal(resourceIdStateNormal);
        cameraFragment.setSwitchCameraResourceIdStatePressed(resourceIdStatePressed);
    }

    /**
     * Sets image/icon for flash on/off toggle
     *
     * @param resourceIdFlashOn  the resource identifier of the drawable for flash on state
     * @param resourceIdFlashOff the resource identifier of the drawable for flash off state
     */
    public static void setFlashImageResource(int resourceIdFlashOn, int resourceIdFlashOff) {
        cameraFragment.setFlashOnResourceId(resourceIdFlashOn);
        cameraFragment.setFlashOffResourceId(resourceIdFlashOff);
    }

    /**
     * Sets icon image for Camera capture button
     *
     * @param resourceId the resource identifier of the drawable
     */
    public static void setCameraCaptureImageResource(int resourceId) {
        cameraFragment.setCameraCaptureResourceIdStateNormal(resourceId);
    }

    /**
     * Sets icon image for Camera capture button
     *
     * @param resourceIdStateNormal  the resource identifier of the drawable for state normal
     * @param resourceIdStatePressed the resource identifier of the drawable for state pressed
     */
    public static void setCameraCaptureImageResource(int resourceIdStateNormal, int resourceIdStatePressed) {
        cameraFragment.setCameraCaptureResourceIdStateNormal(resourceIdStateNormal);
        cameraFragment.setCameraCaptureResourceIdStatePressed(resourceIdStatePressed);
    }

    /**
     * Sets background color for the buttons container layout below the camera preview
     *
     * @param context
     * @param backgroundColor the color of the background
     */
    public static void setBackgroundColor(Context context, int backgroundColor) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        cameraFragment.setBackgroundColor(ContextCompat.getColor(context, backgroundColor));
    }

    /**
     * sets background image for the buttons container layout below the camera preview
     *
     * @param resourceId The identifier of the resource.
     */
    public static void setBackgroundImage(int resourceId) {
        cameraFragment.setBackgroundImage(resourceId);
    }

    /**
     * Sets separator line color and makes the separator line visible
     *
     * @param context
     * @param color - color to be set to the separator line
     */
    public static void setSeparatorLineColor(Context context, int color) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        cameraFragment.setSeparatorLineColor(ContextCompat.getColor(context, color));
    }

    /**
     * sets toast message for devices with no flash feature
     *
     * @param toastMessageNoFlash message to display in the toast
     */
    public static void setToastMessageNoFlash(String toastMessageNoFlash) {
        cameraFragment.setToastMessageNoFlash(toastMessageNoFlash);
    }

    /**
     * sets toast message for devices with no front camera
     *
     * @param toastMessageNoFrontCam  message to display in the toast
     */
    public static void setToastMessageNoFrontCam(String toastMessageNoFrontCam) {
        cameraFragment.setToastMessageNoFrontCam(toastMessageNoFrontCam);
    }

    /**
     * Loads Camera fragment into the containerView that is supplied.
     *
     * @param containerViewId  Id of the container layout in which the camera fragment is to be loaded.
     * @param fragmentTransaction if loading from Activity - Pass getSupportFragmentManager().beginTransaction() and
     *                            if loading inside another fragment - Pass getChildFragmentManager().beginTransaction()
     * @param creativeCameraListener Listener to receive a callback with IMAGE PATH when picture is captured.
     */
    public static void loadCameraFragment(int containerViewId, FragmentTransaction fragmentTransaction, CameraFragment.CreativeCameraListener creativeCameraListener) {
        cameraFragment.setCreativeCameraListenerImagePath(creativeCameraListener);
        fragmentTransaction.replace(containerViewId, cameraFragment);
        fragmentTransaction.commit();
    }

}