package com.propaganda3.p3camera.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.propaganda3.p3camera.R;
import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.ui.adapter.CameraRollAdapter;

import java.util.List;

/**
 * Created by nikhil on 20/10/15.
 */
public class CameraRollFragment extends Fragment {

    private List<ImageData> imageData;
    public static final String INTENT_EXTRA_PHOTO_WIDTH = "photoWidth";
    public static final String INTENT_EXTRA_PHOTO_HEIGHT = "photoHeight";
    private int backgroundColor = 0, backgroundResourceId = 0;
    private int photoHeight = 640, photoWidth = 640;

    private CreativeCameraRollListener creativeCameraRollListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.p3_activity_camera_roll, container, false);

        if(backgroundColor != 0){
            view.setBackgroundColor(backgroundColor);
        }
        if(backgroundResourceId!=0){
            view.setBackgroundResource(backgroundResourceId);
        }

        GridView gridViewCameraImages = (GridView) view.findViewById(R.id.camera_roll_grid_view);

        gridViewCameraImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                creativeCameraRollListener.onPictureSelected(imageData.get(position).getPath(), imageData.get(position).getRotation());
            }
        });

        CameraRollAdapter cameraRollAdapter = new CameraRollAdapter(getActivity(), imageData);
        gridViewCameraImages.setAdapter(cameraRollAdapter);
        return view;

    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBackgroundResourceId(int backgroundResourceId) {
        this.backgroundResourceId = backgroundResourceId;
    }

    public void setImageData(List<ImageData> imageData) {
        this.imageData = imageData;
    }

    public void setPhotoHeight(int photoHeight) {
        this.photoHeight = photoHeight;
    }

    public void setPhotoWidth(int photoWidth) {
        this.photoWidth = photoWidth;
    }

    public void setCreativeCameraRollListener(CreativeCameraRollListener creativeCameraRollListener) {
        this.creativeCameraRollListener = creativeCameraRollListener;
    }

    public interface CreativeCameraRollListener{
        void onPictureSelected(String imagePath, int imageRotation);
    }
}
