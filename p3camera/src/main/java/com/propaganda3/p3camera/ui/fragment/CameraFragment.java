package com.propaganda3.p3camera.ui.fragment;


import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.StateListDrawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.propaganda3.p3camera.R;
import com.propaganda3.p3camera.manager.P3CameraSharedPreferencesManager;
import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.task.DecodeImageCameraTask;
import com.propaganda3.p3camera.ui.customview.P3CameraPreviewSurfaceView;
import com.propaganda3.p3camera.utility.BitmapHelper;
import com.propaganda3.p3camera.utility.P3CameraCommon;
import com.propaganda3.p3camera.utility.P3CameraConstant;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by nikhil on 09/10/15.
 */
public class CameraFragment extends Fragment implements View.OnClickListener{

    private String toastMessageNoFlash;
    private String toastMessageNoFrontCam;
    public  static final String INTENT_EXTRA_CAMERA_FRAGMENT_IMAGE_DATA_LIST = "cameraFragmentImageDataList";

    private ImageButton imageButtonSwitchCamera;
    private ImageButton imageButtonClickPhoto;
    private ImageView imageViewLastUsedPicture;
    private CheckBox checkBoxToggleFlash;
    private View viewSeparatorLine;

    private FrameLayout cameraPreviewLayout;

    private Camera camera;
    private P3CameraPreviewSurfaceView p3CameraPreviewSurfaceView;

    private boolean backCameraInUse;

    private int photoHeight = 640;
    private int photoWidth = 640;

    private ArrayList<ImageData> imageDataList;

    private int switchCameraResourceIdStateNormal , switchCameraResourceIdStatePressed = 0, flashOnResourceId = 0,
            flashOffResourceId = 0, cameraCaptureResourceIdStateNormal ,cameraCaptureResourceIdStatePressed = 0, backgroundColor = 0, backgroundResourceId = 0, separatorLineColor = 0;
    private CreativeCameraListener creativeCameraListener;
    LinearLayout linearLayoutButtons;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.p3_fragment_camera, container, false);
        toastMessageNoFlash = getString(R.string.p3_camera_toast_message_no_flash_feature);
        toastMessageNoFrontCam = getString(R.string.p3_camera_toast_message_no_front_camera);
        setupViews(view);
        return view;
    }


    // Linking the id to their views
    private void setupViews(View view) {

        cameraPreviewLayout = (FrameLayout) view.findViewById(R.id.camera_preview);
        imageButtonSwitchCamera = (ImageButton) view.findViewById(R.id.cam_image_button_switch_camera);
        checkBoxToggleFlash = (CheckBox) view.findViewById(R.id.cam_checkbox_toggle_flash);
        imageButtonClickPhoto = (ImageButton) view.findViewById(R.id.cam_image_button_snap);
        imageViewLastUsedPicture = (ImageView) view.findViewById(R.id.cam_image_view_last_image);
        linearLayoutButtons = (LinearLayout) view.findViewById(R.id.cam_linear_buttons);
        viewSeparatorLine = view.findViewById(R.id.camera_view_separator_line);

        imageButtonSwitchCamera.setOnClickListener(this);
        imageButtonClickPhoto.setOnClickListener(this);
        imageViewLastUsedPicture.setOnClickListener(this);
        setResources();
        checkBoxToggleFlash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleFlash(isChecked);
            }
        });

    }

    private void setResources() {
        if(switchCameraResourceIdStatePressed != 0) {
            imageButtonSwitchCamera.setImageDrawable(getButtonStateListDrawable(switchCameraResourceIdStateNormal, switchCameraResourceIdStatePressed));
        }else{
            imageButtonSwitchCamera.setImageResource(switchCameraResourceIdStateNormal);
        }
        if(cameraCaptureResourceIdStatePressed != 0) {
            imageButtonClickPhoto.setImageDrawable(getButtonStateListDrawable(cameraCaptureResourceIdStateNormal, cameraCaptureResourceIdStatePressed));
        }else{
            imageButtonClickPhoto.setImageResource(cameraCaptureResourceIdStateNormal);
        }

        if(backgroundColor != 0){
            linearLayoutButtons.setBackgroundColor(backgroundColor);
        }
        if(backgroundResourceId != 0){
            linearLayoutButtons.setBackgroundResource(backgroundResourceId);
        }
        if(separatorLineColor != 0){
            viewSeparatorLine.setVisibility(View.VISIBLE);
            viewSeparatorLine.setBackgroundColor(separatorLineColor);
        }
        checkBoxToggleFlash.setButtonDrawable(getCheckBoxStateListDrawable(flashOnResourceId, flashOffResourceId));
    }

    /**
     * Used to enable or disable the buttons
     *
     * @param flag - send true to enable button and false to disable buttons.
     */
    private void enableButtons(boolean flag)
    {
        imageButtonSwitchCamera.setEnabled(flag);
        imageButtonClickPhoto.setEnabled(flag);
        imageViewLastUsedPicture.setEnabled(flag);
        checkBoxToggleFlash.setEnabled(flag);
    }

    @Override
    public void onResume() {
        super.onResume();
        //imageData = null;
        toggleFlashButtonVisibility(View.VISIBLE);
        showBackCamera();
        displayLastUsedImage();
        enableButtons(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        removePreview();
        releaseCamera();
    }


    private void showBackCamera() {
        releaseCamera();
        camera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_BACK);
        if (camera != null) {
            if (p3CameraPreviewSurfaceView != null) {
                removePreview();
            }
            if(checkBoxToggleFlash.isChecked()) {
                toggleFlash(true);//turnOnFlash();
            }
            backCameraInUse = true;
            attachCameraToView();
        }
    }


    private void showFrontCamera() {
        int numberOfCameras = Camera.getNumberOfCameras();

        if (numberOfCameras > 1) {
            releaseCamera();
            camera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_FRONT);
            backCameraInUse = false;
            removePreview();
            attachCameraToView();
            toggleFlashButtonVisibility(View.INVISIBLE);
        } else {
            Toast.makeText(getActivity(), toastMessageNoFrontCam, Toast.LENGTH_SHORT).show();
        }
    }


    private void toggleFlashButtonVisibility(int visibility) {
        checkBoxToggleFlash.setVisibility(visibility);
    }

    private void displayLastUsedImage() {
        imageDataList = P3CameraCommon.getCameraImagePaths(getActivity());

        if(imageDataList.size() > 0) {
            ImageData data = imageDataList.get(0);
            File imageFile = new File(data.getPath());

            if (imageFile.exists()) {
                //get a scaled down version of the image to show in the thumbnail
                Bitmap myBitmap = BitmapHelper.decodeSampledBitmapFromImageData(data, P3CameraConstant.THUMBNAIL_IMAGE_WIDTH, P3CameraConstant.THUMBNAIL_IMAGE_HEIGHT);

                imageViewLastUsedPicture.setImageBitmap(myBitmap);
            }
        }

    }

    private void releaseCamera() {
        if (camera != null) {
            camera.release(); // release the camera for other applications
            camera = null;
        }
    }

    private void removePreview() {
        cameraPreviewLayout.removeView(p3CameraPreviewSurfaceView);
    }


    private void attachCameraToView() {
        p3CameraPreviewSurfaceView = new P3CameraPreviewSurfaceView(getActivity(), camera);
        cameraPreviewLayout.addView(p3CameraPreviewSurfaceView);
    }

    private Camera getCameraInstance(int cameraID) {
        Camera c = null;
        try {
            c = Camera.open(cameraID); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable

    }


    //callback for handling picture taken by camera
    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            //enableButtons(true);
            //imageData = data;
            camera.stopPreview();

            if (data != null) {
                new DecodeImageCameraTask(data,
                        cameraPreviewLayout.getHeight(),
                        p3CameraPreviewSurfaceView.getHeight(), photoWidth, photoHeight,
                        backCameraInUse,
                        new DecodeImageCameraTask.DecodeBitmapListener() {
                            @Override
                            public void onDecodeBitmapCompletion(Bitmap bitmap) {
                                enableButtons(true);
                                if(creativeCameraListener != null){
                                    //Rotation is 0 since Camera orientation is fixed to portrait mode
                                    creativeCameraListener.onPictureTaken(P3CameraCommon.saveImageBitmapToExternalStorage(P3CameraSharedPreferencesManager.getPrefStorageFolderName(getActivity()), P3CameraConstant.CAMERA_CAPTURE_TEMP_IMAGE_NAME, bitmap),0);
                                }
                                bitmap = null;
                            }
                        }).execute();
            }

        }
    };


    private void toggleFlash(boolean value) {

        if (isFlashSupported()) {
            Camera.Parameters parameters = camera.getParameters();
            //checkBoxToggleFlash.setre
            parameters.setFlashMode(value ? Camera.Parameters.FLASH_MODE_ON : Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);

            if (value) {
                camera.startPreview();
            }
        } else {
            Toast.makeText(getActivity(), toastMessageNoFlash, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isFlashSupported() {
        // if device support camera flash?
        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.cam_image_button_switch_camera) {
            enableButtons(false);
            if (backCameraInUse) {
                showFrontCamera();
            } else {
                showBackCamera();
                checkBoxToggleFlash.setVisibility(View.VISIBLE);
            }
            enableButtons(true);

        } else if (i == R.id.cam_image_button_snap) {
            enableButtons(false);
            camera.takePicture(null, null, pictureCallback);

        } else if (i == R.id.cam_image_view_last_image) {
            enableButtons(false);
            creativeCameraListener.onCameraRollButtonClicked(imageDataList);
        } else {
        }
    }

    private StateListDrawable getButtonStateListDrawable(int resourceIdStateNormal, int resourceIdStatePressed){
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[] {android.R.attr.state_pressed},
                ContextCompat.getDrawable(getActivity(),resourceIdStatePressed));
        stateListDrawable.addState(new int[] { },
                ContextCompat.getDrawable(getActivity(), resourceIdStateNormal));
        return stateListDrawable;
    }

    private StateListDrawable getCheckBoxStateListDrawable(int resourceIdStateChecked, int resourceIdStateUnChecked){
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[] {android.R.attr.state_checked},
                ContextCompat.getDrawable(getActivity(),resourceIdStateChecked));
        stateListDrawable.addState(new int[] { },
                ContextCompat.getDrawable(getActivity(), resourceIdStateUnChecked));
        return stateListDrawable;
    }

    public void setSwitchCameraResourceIdStateNormal(int switchCameraResourceIdStateNormal) {
        this.switchCameraResourceIdStateNormal = switchCameraResourceIdStateNormal;
    }

    public void setSwitchCameraResourceIdStatePressed(int switchCameraResourceIdStatePressed) {
        this.switchCameraResourceIdStatePressed = switchCameraResourceIdStatePressed;
    }

    public void setFlashOnResourceId(int flashOnResourceId) {
        this.flashOnResourceId = flashOnResourceId;
    }

    public void setFlashOffResourceId(int flashOffResourceId) {
        this.flashOffResourceId = flashOffResourceId;
    }

    public void setCameraCaptureResourceIdStateNormal(int cameraCaptureResourceIdStateNormal) {
        this.cameraCaptureResourceIdStateNormal = cameraCaptureResourceIdStateNormal;
    }

    public void setCameraCaptureResourceIdStatePressed(int cameraCaptureResourceIdStatePressed) {
        this.cameraCaptureResourceIdStatePressed = cameraCaptureResourceIdStatePressed;
    }

    public void setPhotoHeight(int photoHeight) {
        this.photoHeight = photoHeight;
    }

    public void setPhotoWidth(int photoWidth) {
        this.photoWidth = photoWidth;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBackgroundImage(int resourceId) {
        this.backgroundResourceId = resourceId;
    }

    public void setToastMessageNoFlash(String toastMessageNoFlash) {
        this.toastMessageNoFlash = toastMessageNoFlash;
    }

    public void setToastMessageNoFrontCam(String toastMessageNoFrontCam) {
        this.toastMessageNoFrontCam = toastMessageNoFrontCam;
    }

    public void setSeparatorLineColor(int separatorLineColor) {
        this.separatorLineColor = separatorLineColor;
    }

    public void setCreativeCameraListenerImagePath(CreativeCameraListener creativeCameraListener) {
        this.creativeCameraListener = creativeCameraListener;
    }

    public interface CreativeCameraListener {

        void onPictureTaken(String imagePath, int imageRotation);
        void onCameraRollButtonClicked(ArrayList<ImageData> imageData);
    }

}