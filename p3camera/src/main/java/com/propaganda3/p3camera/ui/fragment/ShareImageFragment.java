package com.propaganda3.p3camera.ui.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.propaganda3.p3camera.R;
import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.model.ShareIcon;
import com.propaganda3.p3camera.utility.BitmapHelper;
import com.propaganda3.p3camera.utility.P3CameraCommon;
import com.propaganda3.p3camera.utility.P3CameraConstant;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nikhil on 19/10/15.
 */
public class ShareImageFragment extends Fragment {

    private static final int SHARE_VIA_TWITTER = 0;
    private static final int SHARE_VIA_FACEBOOK = 1;
    private static final int SHARE_VIA_INSTAGRAM = 2;
    private static final int SHARE_VIA_EMAIL = 3;
    private static final int SAVE_IMAGE = 4;
    private EditText editTextCaptionText;
    private String hashTag = "";
    private TextView textViewHashTag;
    private List<ShareIcon> listShareIconsOrder = new ArrayList<>();
    private String saveToAlbumDirectoryPath, saveToAlbumImageName;
    private ImageButton imageButton1 , imageButton2 ,imageButton3 ,imageButton4 , imageButton5;
    private String clickedImagePath;
    private final String IMAGE_EXTENSION = ".jpg";
    private ImageView sharePictureImageView;
    private int hashTagTextColor = 0, hashTagTextSize = 0, captionTextColor = 0,  captionTextHintColor = 0, captionTextSize = 0;
    private Typeface  hashTagTextTypeFace = null, captionTextTypeFace = null;
    private int backgroundColorScreen = 0, backgroundColorImageAndCaption = 0;
    private String toastMessageImageSaved = null, captionTextHint = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.p3_fragment_share_image, container, false);
        clickedImagePath = getArguments().getString(P3CameraConstant.INTENT_EXTRA_IMAGE_PATH);
        setupViews(view);
        displayClickedImage();
        return view;

    }

    private void setupViews(View view) {
        sharePictureImageView = (ImageView) view.findViewById(R.id.share_image_thumbnail);
        editTextCaptionText = (EditText) view.findViewById(R.id.share_edit_caption);
        imageButton1 = (ImageButton) view.findViewById(R.id.share_image_1);
        imageButton2 = (ImageButton) view.findViewById(R.id.share_image_2);
        imageButton3 = (ImageButton) view.findViewById(R.id.share_image_3);
        imageButton4 = (ImageButton) view.findViewById(R.id.share_image_4);
        imageButton5 = (ImageButton) view.findViewById(R.id.share_image_5);

        textViewHashTag = (TextView) view.findViewById(R.id.share_text_hashtag);
        textViewHashTag.setText(hashTag);

        if(hashTagTextSize != 0){
            textViewHashTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, hashTagTextSize);
        }
        if(hashTagTextColor != 0){
            textViewHashTag.setTextColor(hashTagTextColor);
        }
        if(hashTagTextTypeFace != null){
            textViewHashTag.setTypeface(hashTagTextTypeFace);
        }
        if(captionTextColor != 0){
            editTextCaptionText.setTextColor(captionTextColor);
        }
        if(captionTextHintColor != 0){
            editTextCaptionText.setHintTextColor(captionTextHintColor);
        }
        if(captionTextSize != 0){
            editTextCaptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, captionTextSize);
        }
        if(captionTextTypeFace != null){
            editTextCaptionText.setTypeface(captionTextTypeFace);
        }
        if(captionTextHint != null){
            editTextCaptionText.setHint(captionTextHint);
        }
        if(backgroundColorScreen != 0) {
            view.setBackgroundColor(backgroundColorScreen);
        }
        if(backgroundColorImageAndCaption != 0) {
            view.findViewById(R.id.share_relative_image_and_caption).setBackgroundColor(backgroundColorImageAndCaption);
        }

        setShareButtonResources();
    }

    private void setShareButtonResources() {

        //list items are copied in another array and the list is cleared so that when this fragment is called again the previous items doesn't remain in the list
        List<ShareIcon> newShareIconsList = new ArrayList<>();
        newShareIconsList.addAll(listShareIconsOrder);
        listShareIconsOrder.clear();

        if(!newShareIconsList.isEmpty()){
            for(int i = 0; i < newShareIconsList.size() ; i++) {

                ShareIcon shareIcon = newShareIconsList.get(i);

                switch (i) {
                    case 0:
                        imageButton1.setVisibility(View.VISIBLE);
                        imageButton1.setImageResource(shareIcon.getIconResourceId());
                        imageButton1.setOnClickListener(getShareButtonOnClickListener(shareIcon.getShareButtonIndex()));
                        break;
                    case 1:
                        imageButton2.setVisibility(View.VISIBLE);
                        imageButton2.setImageResource(shareIcon.getIconResourceId());
                        imageButton2.setOnClickListener(getShareButtonOnClickListener(shareIcon.getShareButtonIndex()));
                        break;
                    case 2:
                        imageButton3.setVisibility(View.VISIBLE);
                        imageButton3.setImageResource(shareIcon.getIconResourceId());
                        imageButton3.setOnClickListener(getShareButtonOnClickListener(shareIcon.getShareButtonIndex()));
                        break;
                    case 3:
                        imageButton4.setVisibility(View.VISIBLE);
                        imageButton4.setImageResource(shareIcon.getIconResourceId());
                        imageButton4.setOnClickListener(getShareButtonOnClickListener(shareIcon.getShareButtonIndex()));
                        break;
                    case 4:
                        imageButton5.setVisibility(View.VISIBLE);
                        imageButton5.setImageResource(shareIcon.getIconResourceId());
                        imageButton5.setOnClickListener(getShareButtonOnClickListener(shareIcon.getShareButtonIndex()));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void displayClickedImage() {

        //ViewTreeObserver is used so that we get ImageView height and width once the layout is created. (Initially the height and width is 0)
        ViewTreeObserver viewTreeObserver = this.sharePictureImageView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {

                if (clickedImagePath != null) {
                    Bitmap bitmap = BitmapHelper.decodeSampledBitmapFromImageData(new ImageData(clickedImagePath, 0), sharePictureImageView.getWidth(), sharePictureImageView.getHeight());
                    sharePictureImageView.setImageBitmap(bitmap);
                    bitmap = null;
                }

                ViewTreeObserver viewTreeObserver = ShareImageFragment.this.sharePictureImageView
                        .getViewTreeObserver();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                } else {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                }
            }
        });
    }

    private View.OnClickListener getShareButtonOnClickListener(int shareButtonIndex) {

        View.OnClickListener onClickListener = null;
        switch (shareButtonIndex) {
            case SHARE_VIA_FACEBOOK:
                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      P3CameraCommon.shareViaFB(getActivity(), Uri.fromFile(new File(clickedImagePath)), editTextCaptionText.getText().toString() + " " + hashTag);
                    }
                };
                break;
            case SHARE_VIA_TWITTER:
                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        P3CameraCommon.shareUsingTwitter(getActivity(), Uri.fromFile(new File(clickedImagePath)), editTextCaptionText.getText().toString() + " " + hashTag);
                    }
                };
                break;
            case SHARE_VIA_INSTAGRAM:
                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        P3CameraCommon.shareUsingInstagram(getActivity(), Uri.fromFile(new File(clickedImagePath)), editTextCaptionText.getText().toString() + " " + hashTag);
                    }
                };
                break;
            case SHARE_VIA_EMAIL:
                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        P3CameraCommon.shareImageUsingEmail(getActivity(), Uri.fromFile(new File(clickedImagePath)), hashTag, editTextCaptionText.getText().toString());
                    }
                };
                break;

            case SAVE_IMAGE:
                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //ProgressSpinner.show(getActivity(), getString(R.string.progress_message_please_wait));
                        File file = new File(saveToAlbumDirectoryPath);
                        if(!file.exists()){
                            file.mkdirs();
                        }
                        String destinationFilePath = (saveToAlbumDirectoryPath.endsWith("/") ? saveToAlbumDirectoryPath : saveToAlbumDirectoryPath + "/") + (saveToAlbumImageName != null ? saveToAlbumImageName : "Image-" + new Date().toString() + IMAGE_EXTENSION);
                        if(P3CameraCommon.copyFile(clickedImagePath, destinationFilePath)){
                            P3CameraCommon.sendBroadcastToRefreshGallery(getActivity(), destinationFilePath);
                            Toast.makeText(getActivity(), toastMessageImageSaved != null ? toastMessageImageSaved : getString(R.string.p3_share_toast_image_saved), Toast.LENGTH_SHORT).show();
                        }
                        //ProgressSpinner.dismiss();
                    }
                };
                break;
            default:
                break;
        }
        return onClickListener;
    }

    public void setAlbumIcon(int iconResourceId, String saveToAlbumDirectoryPath, String saveToAlbumImageName) {
        if(saveToAlbumImageName != null){
            this.saveToAlbumImageName = saveToAlbumImageName;
        }
        this.saveToAlbumDirectoryPath = saveToAlbumDirectoryPath;
        listShareIconsOrder.add(new ShareIcon(SAVE_IMAGE, iconResourceId));
    }

    public void setEmailIcon(int iconResourceId) {
        listShareIconsOrder.add(new ShareIcon(SHARE_VIA_EMAIL, iconResourceId));
    }

    public void setTwitterIcon(int iconResourceId) {
        listShareIconsOrder.add(new ShareIcon(SHARE_VIA_TWITTER, iconResourceId));
    }

    public void setFacebookIcon(int iconResourceId) {
        listShareIconsOrder.add(new ShareIcon(SHARE_VIA_FACEBOOK, iconResourceId));
    }

    public void setInstagramIcon(int iconResourceId) {
        listShareIconsOrder.add(new ShareIcon(SHARE_VIA_INSTAGRAM, iconResourceId));
    }

    public void setHashTag(String hashTag, int hashTagTextColor, Typeface hashTagTextTypeFace, int hashTagTextSize) {
        this.hashTag = hashTag;
        this.hashTagTextColor = hashTagTextColor;
        this.hashTagTextSize = hashTagTextSize;
        this.hashTagTextTypeFace = hashTagTextTypeFace;
    }

    public void setCaptionTextFont(String hintText, int hintTextColor, int textColor, Typeface typeface, int textSize) {
        this.captionTextHint = hintText;
        this.captionTextSize = textSize;
        this.captionTextTypeFace = typeface;
        this.captionTextColor = textColor;
        this.captionTextHintColor = hintTextColor;
    }

    public void setToastMessageImageSaved(String toastMessageImageSaved) {
        this.toastMessageImageSaved = toastMessageImageSaved;
    }

    public void setBackgroundColorScreen(int backgroundColorScreen) {
        this.backgroundColorScreen = backgroundColorScreen;
    }

    public void setBackgroundColorImageAndCaption(int backgroundColorImageAndCaption) {
        this.backgroundColorImageAndCaption = backgroundColorImageAndCaption;
    }
}
