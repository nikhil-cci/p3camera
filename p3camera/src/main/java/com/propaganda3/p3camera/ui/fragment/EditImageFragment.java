package com.propaganda3.p3camera.ui.fragment;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.propaganda3.p3camera.R;
import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.ui.customview.FrameImageView;
import com.propaganda3.p3camera.utility.BitmapHelper;
import com.propaganda3.p3camera.utility.P3CameraConstant;

/**
 * Created by nikhil on 14/10/15.
 */
public class EditImageFragment extends Fragment{
    private static final int MASK_THUMB_WIDTH = 90;
    private static final int MASK_THUMB_HEIGHT = 90;
    private static final int MASK_THUMB_PADDING = 1;
    private static final int MASK_THUMB_MARGIN = 8;

    private RelativeLayout framePreviewLayout;
    private ImageView clickedPictureImageView;
    private FrameImageView maskImageView;
    private LinearLayout maskImageContainer;
    private boolean displayImageInTheCentre = true;

    private int[] frameResourceIds, frameThumbResourceIds;
    private int badgesStartPosition;
    private int backgroundColor = 0, backgroundResourceId = 0, separatorViewColor = 0, backgroundFrameThumb = 0, backgroundRemoveMaskThumbText = 0;
    private Typeface typefaceThumbRemoveMaskText;
    private String removeMaskThumbText;
    EditImageListener editImageListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.p3_fragment_edit_image, container, false);
        setupViews(view);
        displayClickedImage();
        return view;

    }

    private void displayClickedImage() {

        //ViewTreeObserver is used so that we get ImageView height and width once the layout is created. (Initially the height and width is 0)
        ViewTreeObserver viewTreeObserver = this.clickedPictureImageView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {

                String imagePath = getArguments().getString(P3CameraConstant.INTENT_EXTRA_IMAGE_PATH);
                int imageRotation = getArguments().getInt(P3CameraConstant.INTENT_EXTRA_IMAGE_ROTATION);

                if (imagePath != null) {
                    Bitmap bitmap = BitmapHelper.decodeSampledBitmapFromImageData(new ImageData(imagePath, imageRotation), clickedPictureImageView.getWidth(), clickedPictureImageView.getHeight());
                    clickedPictureImageView.setImageBitmap(bitmap);
                    bitmap = null;
                }

                ViewTreeObserver viewTreeObserver = EditImageFragment.this.clickedPictureImageView
                        .getViewTreeObserver();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                } else {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                }
            }
        });
    }

    private void setupViews(View view) {
        framePreviewLayout = (RelativeLayout) view.findViewById(R.id.edit_image_frame_preview);
        clickedPictureImageView = (ImageView) view.findViewById(R.id.edit_image_view_picture);
        maskImageView = (FrameImageView) view.findViewById(R.id.edit_image_image_mask);
        maskImageContainer = (LinearLayout) view.findViewById(R.id.edit_image_mask_image_container);
        maskImageView.setEnabled(false);

        TextView textViewRemoveMask = (TextView) view.findViewById(R.id.edit_text_remove_mask_thumb);
        if(removeMaskThumbText != null){
            textViewRemoveMask.setText(removeMaskThumbText);
        }
        if(typefaceThumbRemoveMaskText != null){
            textViewRemoveMask.setTypeface(typefaceThumbRemoveMaskText);
        }
        if(backgroundRemoveMaskThumbText != 0){
            textViewRemoveMask.setBackgroundResource(backgroundRemoveMaskThumbText);
        }

        textViewRemoveMask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maskImageView.setImageBitmap(null);
            }
        });

        HorizontalScrollView horizontalScrollViewFrames = (HorizontalScrollView) view.findViewById(R.id.edit_image_mask_scroll_view);
        View viewSeparatorLine = view.findViewById(R.id.edit_image_view_separator_line);

        if(backgroundColor!= 0) {
            horizontalScrollViewFrames.setBackgroundColor(backgroundColor);
        }
        if(backgroundResourceId != 0){
            horizontalScrollViewFrames.setBackgroundResource(backgroundResourceId);
        }

        if(separatorViewColor != 0){
            viewSeparatorLine.setVisibility(View.VISIBLE);
            viewSeparatorLine.setBackgroundColor(separatorViewColor);
        }

        createMaskImageViews();

    }

    private void createMaskImageViews() {

        //create image views to display mask
        for (int i = 0; i < frameThumbResourceIds.length; ++i) {

            ImageView maskImageView = new ImageView(getActivity());
            //int imageNumber = i + 1;
            //String resourceName = "frame_" + imageNumber + "_thumb";
            //int resourceID = P3CameraCommon.getResourceID(getActivity(), resourceName);
            maskImageView.setImageResource(frameThumbResourceIds[i]);
            maskImageView.setId(i);
            maskImageView.setTag(String.valueOf(i));

            //display the image in the centre
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(convertDpToPixels(MASK_THUMB_WIDTH), convertDpToPixels(MASK_THUMB_HEIGHT));
            params.setMargins(0, 0, convertDpToPixels(MASK_THUMB_MARGIN), 0);
            maskImageView.setPadding(convertDpToPixels(MASK_THUMB_PADDING), convertDpToPixels(MASK_THUMB_PADDING), convertDpToPixels(MASK_THUMB_PADDING), convertDpToPixels(MASK_THUMB_PADDING));
            maskImageView.setLayoutParams(params);
            if(backgroundFrameThumb != 0) {
                maskImageView.setBackgroundResource(backgroundFrameThumb);
            }

            maskImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMaskImage(v);
                }
            });

            maskImageContainer.addView(maskImageView);
        }
    }

    private void showMaskImage(View view) {
        //get the resource id using the name
        //frameId = (String) view.getTag();

        //remove the view first and add it later to prevent IllegalStateException
        framePreviewLayout.removeView(maskImageView);

        //only few masks are scalable
        int tag = Integer.parseInt((String) (view.getTag()));
        String resourceName = "frame_" + (tag+1);
        int resourceID = frameResourceIds[view.getId()]; //= P3CameraCommon.getResourceID(getActivity(), resourceName);

        if (view.getId() + 1 > badgesStartPosition) {

            if (displayImageInTheCentre) {
                //maskImageView = new CustomImageView(this, null);
            }
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            maskImageView.setEnabled(true);
            maskImageView.setLayoutParams(params);
            maskImageView.setScaleType(ImageView.ScaleType.MATRIX);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resourceID);

//            float scaleX = getScaleFactor(maskImageView.getWidth(), bitmap.getWidth());
//            float scaleY = getScaleFactor(maskImageView.getHeight(), bitmap.getHeight());
//            Matrix matrix = maskImageView.getImageMatrix();
//            matrix.postScale(scaleX, scaleY);
//            maskImageView.setImageMatrix(matrix);


            //translating the image to the centre
            if (displayImageInTheCentre) {
                Matrix matrix = maskImageView.getImageMatrix();
                Matrix myMatrix = matrix;
                float centerX = maskImageView.getWidth() / 2 + bitmap.getWidth() / 6;
                float centerY = maskImageView.getHeight() / 2 + bitmap.getHeight() / 6;
                float scaleX = getScaleFactor(maskImageView.getWidth(), bitmap.getWidth());
                float scaleY = getScaleFactor(maskImageView.getHeight(), bitmap.getHeight());

                float translateY;
                if(scaleX<scaleY) {
                    myMatrix.postScale(scaleX, scaleX);
                    translateY = ((maskImageView.getHeight() - bitmap.getHeight()) / 20) / scaleX;
                } else{
                    myMatrix.postScale(scaleY, scaleY);
                    translateY = ((maskImageView.getHeight() - bitmap.getHeight()) / 20) / scaleY;
                }
                myMatrix.postTranslate(0, translateY);
                matrix = myMatrix;
                maskImageView.setImageMatrix(matrix);

                //myMatrix.postTranslate(centerX, centerY);
                maskImageView.setMatrix(matrix);
                displayImageInTheCentre = false;
                //maskImageView.setImageMatrix(matrix);
            }

        } else {
            displayImageInTheCentre = true;
            maskImageView.setEnabled(false);
            maskImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        maskImageView.setImageResource(resourceID);
        framePreviewLayout.addView(maskImageView);
    }

    public int convertDpToPixels(int dp) {
        Resources resources = getActivity().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int) (dp * (metrics.densityDpi / 160f));
        return px;
    }

    private float getScaleFactor(int viewWidth, int bitmapWidth) {
        return (float) viewWidth / bitmapWidth;
    }

    //this listener will be returned to the activity or fragment which is loading this fragment since next button is not accessible from library.
    private View.OnClickListener onNextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String savedImagePath = BitmapHelper.createBitmapFromView(getActivity(), framePreviewLayout);
           if(savedImagePath != null){
               if(editImageListener != null){
                   editImageListener.onImageSaved(savedImagePath);
               }
           }
        }
    };

    public View.OnClickListener getOnNextClickListener() {
        return onNextClickListener;
    }

    public void setFrameThumbResourceIds(int[] frameThumbResourceIds) {
        this.frameThumbResourceIds = frameThumbResourceIds;
    }

    public void setFrameResourceIds(int[] frameResourceIds) {
        this.frameResourceIds = frameResourceIds;
    }

    public void setBadgesStartPosition(int badgesStartPosition) {
        this.badgesStartPosition = badgesStartPosition;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBackgroundResource(int backgroundResourceId) {
        this.backgroundResourceId = backgroundResourceId;
    }

    public void setSeparatorViewColor(int separatorViewColor) {
        this.separatorViewColor = separatorViewColor;
    }

    public void setTypefaceThumbRemoveMaskText(Typeface typefaceThumbRemoveMaskText) {
        this.typefaceThumbRemoveMaskText = typefaceThumbRemoveMaskText;
    }

    public void setRemoveMaskThumbText(String removeMaskThumbText) {
        this.removeMaskThumbText = removeMaskThumbText;
    }

    public void setBackgroundFrameThumb(int backgroundFrameThumb) {
        this.backgroundFrameThumb = backgroundFrameThumb;
    }

    public void setBackgroundRemoveMaskThumbText(int backgroundRemoveMaskThumbText) {
        this.backgroundRemoveMaskThumbText = backgroundRemoveMaskThumbText;
    }

    public interface EditImageListener{
        void onImageSaved(String savedImagePath);
    }

    public void setEditImageListener(EditImageListener editImageListener) {
        this.editImageListener = editImageListener;
    }
}
