package com.propaganda3.p3camera.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import com.propaganda3.p3camera.R;
import com.propaganda3.p3camera.model.ImageData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class P3CameraCommon {

    public static ArrayList<ImageData> getCameraImagePaths(Context context) {
        final String[] projection = {MediaStore.Images.Media.DATA, "orientation"};
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
        ArrayList<ImageData> result = new ArrayList<ImageData>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                ImageData imageData = new ImageData();
                String data = cursor.getString(dataColumn);
                int rotation = cursor.getInt(1);
                imageData.setPath(data);
                imageData.setRotation(rotation);
                result.add(imageData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;

    }

    public static String saveImageToExternalStorage(String directoryName, String imageName, byte[] data) {
        String root = Environment.getExternalStorageDirectory().toString();
        String directoryPath = root + "/"+directoryName;
        File myDir = new File(directoryPath);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        //String name = "Image-" + new Date().toString() + ".jpg";
        File file = new File(myDir, imageName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            //savedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.write(data);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return directoryPath+ File.separator +imageName;
    }

    public static String saveImageBitmapToExternalStorage(String directoryName, String imageName, Bitmap bitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        String directoryPath = root + "/"+directoryName;
        File myDir = new File(directoryPath);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        //String name = "Image-" + new Date().toString() + ".jpg";
        File file = new File(myDir, imageName);
        if (file.exists()) {
            file.delete();
        }

        //creating .nomedia file to hide the contents of this folder from gallery
        File nomedia = new File(directoryPath + File.separator + ".nomedia");
        if(!nomedia.exists()){
            try {
                nomedia.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //saving file
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            bitmap = null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return directoryPath+ File.separator +imageName;
    }

    public static boolean facebookAppInstalledOnDevice(Context context) {

        boolean isFacebookInstalled = false;
        try {
            context.getPackageManager().getApplicationInfo(
                    P3CameraConstant.FACEBOOK_PACKAGE_NAME, 0);
            isFacebookInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }

        if (isFacebookInstalled) {
            // IF App is installed check if it is enabled or not
            int getAppEnabledInfo = context.getPackageManager()
                    .getApplicationEnabledSetting(
                            P3CameraConstant.FACEBOOK_PACKAGE_NAME);
            if (getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                    || getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static boolean twitterInstalledOnDevice(Context context) {

        boolean isTwitterInstalled = false;
        try {
            context.getPackageManager().getApplicationInfo(P3CameraConstant.TWITTER_PACKAGE_NAME, 0);
            isTwitterInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        if (isTwitterInstalled) {
            //IF App is installed check if it is enabled or not
            int getAppEnabledInfo = context.getPackageManager().getApplicationEnabledSetting(P3CameraConstant.TWITTER_PACKAGE_NAME);
            if (getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_ENABLED || getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static boolean instagramAppInstalledOnDevice(Context context) {

        boolean isInstagramInstalled = false;
        try {
            context.getPackageManager().getApplicationInfo(
                    P3CameraConstant.INSTAGRAM_PACKAGE_NAME, 0);
            isInstagramInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }

        if (isInstagramInstalled) {
            //IF App is installed check if it is enabled or not
            int getAppEnabledInfo = context.getPackageManager().getApplicationEnabledSetting(P3CameraConstant.INSTAGRAM_PACKAGE_NAME);
            if (getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_ENABLED || getAppEnabledInfo == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void shareViaFB(Context context, Uri uri, String message) {
        if(facebookAppInstalledOnDevice(context)) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_TEXT, message);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setPackage(P3CameraConstant.FACEBOOK_PACKAGE_NAME);
            context.startActivity(intent);
        }else{
            showAlertDialog(context,null, context.getString(R.string.p3_share_alert_app_not_installed_description_facebook));
            //showCustomDialogOkButton(context, context.getString(R.string.share_alert_app_not_installed_description_facebook), null, null);
        }
    }


    public static void shareUsingTwitter(Context context, Uri uri, String message) {
        if (twitterInstalledOnDevice(context)) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_TEXT, message);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setPackage(P3CameraConstant.TWITTER_PACKAGE_NAME);
            context.startActivity(intent);
        }else{
            showAlertDialog(context,null, context.getString(R.string.p3_share_alert_app_not_installed_description_twitter));
            //showCustomDialogOkButton(context, context.getString(R.string.share_alert_app_not_installed_description_twitter), null, null);
        }
    }

    public static void shareUsingInstagram(Context context, Uri uri, String message) {
        if (instagramAppInstalledOnDevice(context)) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_TEXT, message);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setPackage(P3CameraConstant.INSTAGRAM_PACKAGE_NAME);
            context.startActivity(intent);
        }else{
            showAlertDialog(context,null, context.getString(R.string.p3_share_alert_app_not_installed_description_instagram));
           // showCustomDialogOkButton(context, context.getString(R.string.share_alert_app_not_installed_description_instagram), null, null);
        }
    }

    public static void shareImageUsingEmail(Context context, Uri uri, String subject, String emailContent) {
        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{" "});
        if(subject!=null) {
            intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if(emailContent!=null) {
            intentEmail.putExtra(Intent.EXTRA_TEXT, emailContent);
        }
        intentEmail.putExtra(Intent.EXTRA_STREAM, uri);
        intentEmail.setType("image/*");
        context.startActivity(Intent.createChooser(intentEmail,
                "Choose an email provider :"));
    }

    public static void sendBroadcastToRefreshGallery(Context context , String filePath) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(new File(filePath)));
            context.sendBroadcast(mediaScanIntent);
        } else {
            context.sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://"
                            + Environment.getExternalStorageDirectory())));
        }
    }

    public static void showAlertDialog(Context context, String title, String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static boolean copyFile(String sourceFilePath, String destinationFilePath){
        boolean success = false;
        try{
            File f1 = new File(sourceFilePath);
            File f2 = new File(destinationFilePath);
            if(!f2.exists()){
                f2.createNewFile();
            }

            InputStream in = new FileInputStream(f1);

            //For Append the file.
            //OutputStream out = new FileOutputStream(f2,true);

            //For Overwrite the file.
            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0){
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            success = true;
        }
        catch(FileNotFoundException ex){
            System.out.println(ex.getMessage() + " in the specified directory.");
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        return success;
    }


    public static int getResourceID(Context context, String resourceName) {
        return context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
    }

}
