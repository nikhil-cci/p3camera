package com.propaganda3.p3camera.utility;

public class P3CameraConstant {

    //<Camera>//
    public static final int CAMERA_ORIENTATION = 90;
    public static final int THUMBNAIL_IMAGE_WIDTH = 100;
    public static final int THUMBNAIL_IMAGE_HEIGHT = 100;
    public static final int FRONT_CAMERA_ROTATION = 270;
    public static final int BACK_CAMERA_ROTATION = 90;
    public static final String CAMERA_CAPTURE_TEMP_IMAGE_NAME = "temp.jpg";

    public static final String INTENT_EXTRA_IMAGE_PATH = "com.creativecapsuleprojects.creativecamera.imagepath";
    public static final String INTENT_EXTRA_IMAGE_ROTATION = "com.creativecapsuleprojects.creativecamera.imagerotation";

    public static final String SHARED_PREFERENCES = "com.creativecapsuleprojects.creativecamera.sharedPreferencesCreativeCamera";
    public static final String SHARED_PREFERENCES_KEY_STORAGE_FOLDER = "com.creativecapsuleprojects.creativecamera.StorageFolderName";

    public static final String TWITTER_PACKAGE_NAME = "com.twitter.android";
    public static final String FACEBOOK_PACKAGE_NAME = "com.facebook.katana";
    public static final String INSTAGRAM_PACKAGE_NAME = "com.instagram.android";


}
