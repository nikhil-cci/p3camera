package com.propaganda3.p3camera.utility;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.task.DecodeImageGalleryTask;

import java.lang.ref.WeakReference;

public class ImageLoader {

    /**
     * Starts an async task to fetch the image
     * @param context The current context
     * @param imageData  The wrapper for path and rotation of the image
     * @param imageView The imageview to pass to the async task
     */
    public static void fetchImage(Context context, ImageData imageData, ImageView imageView){
        if(cancelPotentialDownload(imageData, imageView)) {
            DecodeImageGalleryTask decodeImageGallaryTask = new DecodeImageGalleryTask(context, imageData, imageView);
            AsyncDrawable asyncDrawable = new AsyncDrawable(decodeImageGallaryTask);
            imageView.setImageDrawable(asyncDrawable);
            decodeImageGallaryTask.execute();
        }
    }

    /**
     * Stops the possible download in progress on this ImageView since a new one is about to start
     * @param imageData  The wrapper for path and rotation of the image
     * @param imageView The associated image view
     * @return true if the task is cancelled, false otherwise.
     */
    private static boolean cancelPotentialDownload(ImageData imageData, ImageView imageView){
        DecodeImageGalleryTask decodeImageGallaryTask = getDecodeImageTask(imageView);
        if(decodeImageGallaryTask != null){
            String imagePath = decodeImageGallaryTask.getImageData().getPath();
            if(imagePath == null || (!imagePath.equals(imageData.getPath()))){
                decodeImageGallaryTask.cancel(true);
            }else {
                // The same URL is already being downloaded.
                return false;
            }

        }
        return true;
    }


    //returns the task associated with the image view
    public static DecodeImageGalleryTask getDecodeImageTask(ImageView imageView){
        if(imageView != null){
            final Drawable drawable = imageView.getDrawable();
            if(drawable instanceof AsyncDrawable){
                AsyncDrawable asyncDrawable = (AsyncDrawable)drawable;
                return  asyncDrawable.getDecodeImageTask();
            }
        }
        return null;
    }


    static class AsyncDrawable extends ColorDrawable {

        private final WeakReference<DecodeImageGalleryTask> decodeImageTaskWeakReference;

        public AsyncDrawable( DecodeImageGalleryTask decodeImageGallaryTask){
            super(Color.GRAY);
            decodeImageTaskWeakReference = new WeakReference<DecodeImageGalleryTask>(decodeImageGallaryTask);
        }

        public DecodeImageGalleryTask getDecodeImageTask(){
            return decodeImageTaskWeakReference.get();
        }
    }
}
