package com.propaganda3.p3camera;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.propaganda3.p3camera.ui.fragment.EditImageFragment;
import com.propaganda3.p3camera.utility.P3CameraConstant;

/**
 * Created by nikhil on 15/10/15.
 */
public class P3CameraEdit {
    private static EditImageFragment editImageFragment = new EditImageFragment();

    /**
     * Sets background color for the frame thumbs container layout below the clicked image
     * @param context application's context
     * @param backgroundColor the color of the background
     */
    public static void setBackgroundColor(Context context, int backgroundColor) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        editImageFragment.setBackgroundColor(ContextCompat.getColor(context,backgroundColor));
    }

    /**
     * sets background image for the frame thumbs container layout below the clicked image
     *
     * @param resourceId The identifier of the resource.
     */
    public static void setBackgroundImage(int resourceId) {
        editImageFragment.setBackgroundResource(resourceId);
    }

    /**
     * Sets separator line color and makes the separator line visible
     *
     * @param context
     * @param color color to be set to the separator line
     */
    public static void setSeparatorLineColor(Context context, int color) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        editImageFragment.setSeparatorViewColor(ContextCompat.getColor(context, color));
    }

    /**
     * Sets background for frame thumb images.
     *
     * @param resourceId The identifier of the resource.
     */
    public static void setBackgroundFrameThumb(int resourceId){
        editImageFragment.setBackgroundFrameThumb(resourceId);
    }

    /**
     * Sets remove-mask thumb text and typeface. This is the first thumbnail in the frames horizontal scroll which is used to remove any frame that are applied to the image
     *
     * @param thumbText text on the remove mask thumb.
     * @param typeface font for the remove mask thumb.
     * @param backgroundResourceId The identifier of the resource to be set as background.
     */
    public static void setRemoveMaskTextFont(String thumbText, Typeface typeface, int backgroundResourceId){
        editImageFragment.setRemoveMaskThumbText(thumbText);
        editImageFragment.setTypefaceThumbRemoveMaskText(typeface);
        editImageFragment.setBackgroundRemoveMaskThumbText(backgroundResourceId);
    }

    /**
     * Loads Edit Image fragment into the containerView that is supplied.
     *
     * @param containerViewId Id of the container layout in which the fragment is to be loaded.
     * @param fragmentTransaction if loading from Activity - Pass getSupportFragmentManager().beginTransaction() and
     *                            if loading inside another fragment - Pass getChildFragmentManager().beginTransaction()
     * @param imagePath path of the saved image clicked from camera.
     * @param imageRotation rotation of the image received from camera or camera roll.
     * @param frameResourceIds resourceIds array of frames.
     * @param frameThumbResourceIds resourceIds array of frame thumbs.
     * @param badgesStartPosition Position in the sequence of frames from where the badge images start.(positioning starts from 1).
     * @param editImageListener callback when image is saved. Image is saved at the same path which was supplied to it.
     * @return onClickListener to be set to NEXT button
     */
    public static View.OnClickListener loadEditImageFragment(int containerViewId, FragmentTransaction fragmentTransaction, String imagePath, int imageRotation, int frameResourceIds[], int frameThumbResourceIds[], int badgesStartPosition, EditImageFragment.EditImageListener editImageListener) {
        editImageFragment.setFrameResourceIds(frameResourceIds);
        editImageFragment.setFrameThumbResourceIds(frameThumbResourceIds);
        editImageFragment.setBadgesStartPosition(badgesStartPosition);
        editImageFragment.setEditImageListener(editImageListener);
        Bundle args = new Bundle();
        args.putString(P3CameraConstant.INTENT_EXTRA_IMAGE_PATH, imagePath);
        args.putInt(P3CameraConstant.INTENT_EXTRA_IMAGE_ROTATION, imageRotation);
        editImageFragment.setArguments(args);
        fragmentTransaction.replace(containerViewId, editImageFragment);
        fragmentTransaction.commit();
        return editImageFragment.getOnNextClickListener();
    }
}
