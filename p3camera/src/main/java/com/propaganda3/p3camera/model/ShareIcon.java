package com.propaganda3.p3camera.model;

/**
 * Created by nikhil on 19/10/15.
 */
public class ShareIcon {
    int shareButtonIndex;
    int iconResourceId;

    public ShareIcon(int shareButtonIndex, int iconResourceId) {
        this.shareButtonIndex = shareButtonIndex;
        this.iconResourceId = iconResourceId;
    }

    public int getShareButtonIndex() {
        return shareButtonIndex;
    }

    public void setShareButtonIndex(int shareButtonIndex) {
        this.shareButtonIndex = shareButtonIndex;
    }

    public int getIconResourceId() {
        return iconResourceId;
    }

    public void setIconResourceId(int iconResourceId) {
        this.iconResourceId = iconResourceId;
    }
}
