package com.propaganda3.p3camera;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.ui.fragment.CameraRollFragment;

import java.util.List;

/**
 * Created by nikhil on 20/10/15.
 */
public class P3CameraRoll {

    private static CameraRollFragment cameraRollFragment = new CameraRollFragment();

    /**
     * Sets the resolution of the output image
     *
     * @param photoHeight height of the output image
     * @param photoWidth  width of the output image
     */
    public static void setImageResolution(int photoHeight, int photoWidth) {
        cameraRollFragment.setPhotoHeight(photoHeight);
        cameraRollFragment.setPhotoWidth(photoWidth);
    }

    /**
     * Sets color to screen background
     * @param context
     * @param backgroundColor color of the background
     */
    public static void setBackgroundColor(Context context, int backgroundColor) {
        //ContextCompat is used because library was unable to identify the color given only the int color id
        cameraRollFragment.setBackgroundColor(ContextCompat.getColor(context, backgroundColor));
    }

    /**
     * Sets drawable to screen background
     * @param backgroundResourceId id of the drawable for background
     */
    public static void setBackgroundResourceId(int backgroundResourceId) {
        cameraRollFragment.setBackgroundResourceId(backgroundResourceId);
    }

    /**
     * Loads Camera Roll fragment into the containerView that is supplied.
     *
     * @param containerViewId Id of the container layout in which the fragment is to be loaded.
     * @param fragmentTransaction if loading from Activity - Pass getSupportFragmentManager().beginTransaction() and
     *                            if loading inside another fragment - Pass getChildFragmentManager().beginTransaction()
     * @param imageData list of images data received from Camera fragment
     */
    public static void loadCameraRollFragment(int containerViewId, FragmentTransaction fragmentTransaction, List<ImageData> imageData, CameraRollFragment.CreativeCameraRollListener creativeCameraRollListener) {
        cameraRollFragment.setCreativeCameraRollListener(creativeCameraRollListener);
        cameraRollFragment.setImageData(imageData);
        fragmentTransaction.replace(containerViewId, cameraRollFragment);
        fragmentTransaction.commit();
    }
}
