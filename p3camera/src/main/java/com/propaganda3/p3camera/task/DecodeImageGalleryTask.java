package com.propaganda3.p3camera.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.propaganda3.p3camera.model.ImageData;
import com.propaganda3.p3camera.utility.BitmapHelper;
import com.propaganda3.p3camera.utility.ImageLoader;

import java.lang.ref.WeakReference;


public class DecodeImageGalleryTask extends AsyncTask<Void, Void, Bitmap> {

    private static final int THUMBNAIL_WIDTH = 80;
    private static final int THUMBNAIL_HEIGHT = 80;

    private Context context;
    private ImageData imageData;
    private final WeakReference<ImageView> imageViewWeakReference;

    public ImageData getImageData() {
        return imageData;
    }

    public void setImageData(ImageData imageData) {
        this.imageData = imageData;
    }

    public DecodeImageGalleryTask(Context context, ImageData imageData, ImageView imageView){
        this.context = context;
        this.imageData = imageData;
        imageViewWeakReference = new WeakReference<ImageView>(imageView);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        return BitmapHelper.decodeSampledBitmapFromImageDataWithSampleSizeEight(imageData, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(isCancelled()){
            bitmap = null;
        }

        //check to see if the weak reference is still around. It will be null if it is garbage collected.
        if(imageViewWeakReference != null && bitmap != null){
            ImageView imageView = imageViewWeakReference.get();
            DecodeImageGalleryTask decodeImageGalleryTask = ImageLoader.getDecodeImageTask(imageView);
            if(this == decodeImageGalleryTask && imageView != null){
                imageView.setImageBitmap(bitmap);
            }
        }
    }

//    public void clearImageList(){
//        for(int i = 0; i < imageList.size(); i++){
//            Bitmap bitmap = imageList.get(i);
//            imageList.remove(i);
//            if(bitmap != null){
//                bitmap.recycle();
//                bitmap = null;
//            }
//        }
//    }
}
