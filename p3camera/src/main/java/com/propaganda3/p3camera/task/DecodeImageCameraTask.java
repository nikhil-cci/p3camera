package com.propaganda3.p3camera.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.propaganda3.p3camera.utility.BitmapHelper;
import com.propaganda3.p3camera.utility.P3CameraConstant;

/**
 * Created by vishnu on 06/07/15.
 */
public class DecodeImageCameraTask extends AsyncTask<Void, Void, Bitmap> {

    private byte[] data;

    private int layoutHeight;
    private int surfaceViewHeight;
    private DecodeBitmapListener decodeBitmapListener;
    private int imageWidth, imageHeight;
    private boolean backCameraInUse;

//    private Bitmap bitmap;

    public DecodeImageCameraTask(byte[] data, int layoutHeight, int surfaceViewHeight, int imageWidth, int imageHeight, boolean backCameraInUse, DecodeBitmapListener decodeBitmapListener){
        this.data = data;
        this.layoutHeight = layoutHeight;
        this.surfaceViewHeight = surfaceViewHeight;
        this.decodeBitmapListener = decodeBitmapListener;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.backCameraInUse = backCameraInUse;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {

        //make a call to the garbage collected to force a garbage collection cycle.
        System.gc();

        //Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0 , data.length);
        Bitmap bitmap = BitmapHelper.decodeSampledBitmapFromByteArray(data, imageWidth, imageHeight);

        //for back camera
        if(backCameraInUse) {
            Bitmap rotatedBitmap = BitmapHelper.rotateBitmap(bitmap, P3CameraConstant.BACK_CAMERA_ROTATION, layoutHeight, surfaceViewHeight);
            bitmap = null;
            bitmap = rotatedBitmap;
        }else{
            //for front camera
            bitmap = BitmapHelper.rotateBitmap(bitmap, P3CameraConstant.FRONT_CAMERA_ROTATION);
            bitmap = BitmapHelper.flip(bitmap);


            final int bitmapWidth = bitmap.getWidth();
            final int bitmapHeight = bitmap.getHeight();

            int croppedHeight = (int) ((float)(bitmapHeight * layoutHeight) / surfaceViewHeight);

            if(croppedHeight > bitmapHeight){
                //do not crop anything
                croppedHeight = bitmapHeight;
            }

            Bitmap finalBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, croppedHeight);
            bitmap = null;
            bitmap = finalBitmap;
        }

        Log.d("decode", "decode_complete");

        this.data = null;
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {

        decodeBitmapListener.onDecodeBitmapCompletion(bitmap);
    }

    public interface DecodeBitmapListener {

        void onDecodeBitmapCompletion(Bitmap bitmap);
    }

}
